# Textbokforing

## [Plain Text Accounting](https://plaintextaccounting.org/) på svenska.

Detta repo är tänkt som en hjälp för att använda Plain Text Accounting under svenska förhållanden.

Plain Text Accounting innebär att man för sin bok i klartext i ett vanligt textdokument, och använder verktyg som läser textfilen och producerar rapporter med mera.

Det finns en rad olika program, varav [ledger](https://www.ledger-cli.org), [hledger](https://hledger.org/) och [beancount](https://github.com/beancount/) är några av de vanligaste.

## Importera bokföring

Ett sätt att komma igång är att importera din bokföring från ditt gamla bokföringsprogram. Här är ett skript för att konvertera SIE4-format, som du kan exportera från bokföringsporgrammet, till ledger-format.

Själv exporterade jag min bokföring från Bokio, och då fick jag den kodad i [CP437](https://en.wikipedia.org/wiki/Code_page_437). Det första jag behövde göra var då att konvertera filen:

`iconv -f CP437 bokforing.se > bokforing.se`

## Konton och kontoklasser

SIE 4-filen innehåller den kontoplan du har valt (exempelvis [BAS 2022](https://www.bas.se/kontoplaner/). I BAS-kontoplanerna har varje konto en kod. Några exempel:

```
1930  Företagskonto
2010  Eget kapital
2641  Ingående moms
3001  Försäljning inom Sverige, 25 % moms
4400  Momspliktiga inköp i Sverige
5340  Stenkol och koks
5410  Förbrukningsinventarier
6230  Datakommunikation
7210  Löner till tjänstemän
8420  Räntekostnader för kortfristiga skulder
... osv
```

Den första siffran i kontonumret säger vilken kontoklass den tillhör:

1. tillgångar
2. skulder och eget kapital
3. intäkter
4. utgifter för varor och material
5. övriga kostnader
6. andra övriga kostnader
7. kostnader för personal m.m.
8. finansiella intäkter och kostnader samt skatte- och resultatkonton

I Plain Text Accounting anger man kontonamnen i klartext efter sina egna behov, och kontoplanen kommer att bestå bara av de konton man använder. Kontona delas även här in i kontoklasser. Hur man lägger upp kontona är helt upp till en själv, men här är några exempel: 

```
tillgångar:bank:företagskonto
tillgångar:bank:annatkonto
tillgångar:kassa
skulder:kontokort
skulder:egetkapital
skulder:moms:ingående
skulder:moms:utgående:25
skulder:moms:utgående:12
intäkter:försäljning:25
intäkter:försäljning:12
kostnader:varuinköp
kostnader:lokaler:hyra
konstader:lokaler:städning
kostnader:försäljning:reklam
kostnader:personal:löner
```

Siffrorna 25 respektive 12 ovan anger momssatser.

## Mappa konton

Innan du kan konvertera bokföringen behöver du göra en mappningsfil, som mappar BAS-konton till de konton du själv vill använda. Om du vill kan du utgå från det exempel som finns med: `accountmap.txt`.

## Konvertera

`awk -f convert-from-sie4.awk bokforing.se > bokforing.dat`

## Testa

`ledger -f bokforing.dat balance`

Om den konverterade bokföringen balanserar som den ska, kommer balansen att visas. Balanserar det inte, visas ett beskrivande felmeddelande.

Om det istället för ett kontonamn står ett kontonummer, betyder det att det använda kontot från SIE 4-filen inte finns i mappningsfilen, `accountmap.txt`. Då kan du helt enkelt redigera filen och lägga till BAS-kontot och ange det konto du vill att det ska mappa mot. Sen kör du konverteringsskriptet igen.

