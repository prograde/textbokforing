BEGIN {
  currency = "kr"
  while(getline a < "accountmap.txt") {
    if(match(a, /^[ 't]*[#$]/)) continue; # ignore empty or commented line
    key = substr(a, 0, 4)
    value = substr(a, 6)
    map[key] = value # map account number to textual account
  }
  RS = "#VER" # set record separator to "#VER"
  FS = "\n" # set field separator to linebreak
}

function printIngaendeBalans() {
  for (i = 1; i < NF; ++i) {
    split($(i), columns, " ")
    thing = columns[1]
    year = columns[2] # year: 0 = this year, -1 = last year

    # print date and ingående balans
    if (thing == "#RAR"){
      dateStr = formatDate(columns[3])
      print dateStr " Ingående balans"
    }
    
    # print rows with ingående balans
    # if (thing == "#IB" || (thing == "#RES" && year == "0")){
    if (thing == "#IB" && year == "0") {
      account = mapAccount(columns[3])
      value = columns[4]
      if (value != 0) {
        if (index(value, "\r")) {
          value = substr(value, 0, length(value)-1)
        }
        print "  " account "\t" value " " currency
      }
    }
  }
  print ""
}

function formatDate(dateStr){
  return substr(dateStr, 0, 4) "-" substr(dateStr, 5, 2) "-" substr(dateStr, 7, 2)
}

function formatDateRow(row) {
  dateStr = substr(row, 0, 8)
  dateStr = formatDate(dateStr)
  return dateStr substr(row, 9)
}

function formatTitleRow(row) {
  sub(/\"V\"/, "", row) # remove "V" from beginning of row
  sub(/\"[0-9]+\"/, "", row) # remove verification number
  sub(/\ +/, "", row) # remove spaces
  gsub(/\"/, "", row) # remove " around verification text
  row = substr(row, 0, length(row)-9) # remove trailing date (date of booking)
  row = formatDateRow(row)
  return row;
}

function formatRow(row) {
  sub(/\r/, "", row) # windows format adds a cr char at end. remove
  sub(/[ \t]*#TRANS /, "", row) # remove #TRANS in beginning of row
  sub(/ \{\} /, "\t", row) # replace {} with tab
  row = mapAccountRow(row)
  return "  " row " " currency
}

function mapAccount(account) {
  if (account in map) return map[account]
  else return account
}

function mapAccountRow(row) {
  account = substr(row, 0,4)
  account = mapAccount(account)
  return account substr(row, 5)
}

NR == 1 {
  printIngaendeBalans()
}

NR > 1 {
  print formatTitleRow($1)
  for(i = 3; i < NF-1; ++i){
    print formatRow($(i))
  }
  print ""
}
